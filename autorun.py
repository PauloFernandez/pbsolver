#!/usr/bin/python
from os import system

def change_value(variable, value, order = False):
    with open('parameters.py', 'r') as parameters_file:
        file_text = parameters_file.read().split('\n')

    if order:
        for l in range(len(file_text)):
            if variable in file_text[l]:
                file_text[l] = "{} = {}".format(variable, value)
    else:
        if isinstance(variable, str):
            for l in range(len(file_text)):
                if variable in file_text[l]:
                    file_text[l] = "{} = {}".format(variable, value)

        if isinstance(variable, float) or isinstance(variable, int):
            for l in range(len(file_text)):
                if variable in file_text[l]:
                    file_text[l] = "{} = {}".format(variable, value)

    with open('parameters.py', 'w') as new_parameters_file:
        new_parameters_file.write('\n'.join(file_text))


change_value("bempp.api.global_parameters.quadrature.near.single_order", 4, order = True)
change_value("bempp.api.global_parameters.quadrature.near.double_order", 4, order = True)

change_value("bempp.api.global_parameters.quadrature.medium.single_order", 3, order = True)
change_value("bempp.api.global_parameters.quadrature.medium.double_order", 3, order = True)

change_value("bempp.api.global_parameters.quadrature.far.single_order", 2, order = True)
change_value("bempp.api.global_parameters.quadrature.far.double_order", 2, order = True)

formulations = ["'juffer'", "'yoon_lenhoff'"]
densities = [2, 4, 8, 16]

for form in formulations:
    change_value("formulation", "{}".format(form))
    
    for dens in densities:
        change_value("mesh_density", dens)

        system('./RUN')

block_sizes = [2, 8, 32, 128, 512, 1024, 2048]
change_value("mesh_density", 16)

for form in formulations:
    for mbs in block_sizes:

        change_value("bempp.api.global_parameters.hmat.max_block_size", mbs)
        change_value("bempp.api.global_parameters.hmat.min_block_size", int(mbs / 100) + 1)

        system('./RUN')


change_value("mesh_density", 4)
for form in formulations:
    change_value("formulation", "{}".format(form))
    
    for quad_order in range(1, 6):
        change_value("bempp.api.global_parameters.quadrature.near.single_order", quad_order, order = True)
        change_value("bempp.api.global_parameters.quadrature.near.double_order", quad_order, order = True)

        change_value("bempp.api.global_parameters.quadrature.medium.single_order", quad_order, order = True)
        change_value("bempp.api.global_parameters.quadrature.medium.double_order", quad_order, order = True)

        change_value("bempp.api.global_parameters.quadrature.far.single_order", quad_order, order = True)
        change_value("bempp.api.global_parameters.quadrature.far.double_order", quad_order, order = True)

        system('./RUN')

