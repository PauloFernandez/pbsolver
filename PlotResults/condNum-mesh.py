from plotset import *

common_conditions = [('mol_name', mol_name), ('quadrature.near.single_order', 4), ('hmat.max_block_size', 2048)]

n_boundary_yoonlh = extract_results('number_of_elements', 
                        common_conditions + 
                        [('formulation', 'yoon_lenhoff')])

n_boundary_juffer = extract_results('number_of_elements', 
                        common_conditions + 
                        [('formulation', 'juffer')])

condnum_yoonlh = extract_results('num_cond',
                        common_conditions +
                        [('formulation', 'yoon_lenhoff')])

condnum_juffer = extract_results('num_cond',
                        common_conditions + 
                        [('formulation', 'juffer')])


cond_numb = plt.figure().add_subplot(111)

cond_numb.plot(n_boundary_yoonlh, condnum_yoonlh, marker='o', label='YL', color='k')
cond_numb.plot(n_boundary_juffer, condnum_juffer, marker='s', label='Juffer', color='k')

cond_numb.set_xlabel('N of Elements')
cond_numb.set_ylabel('Condition Number')
cond_numb.legend(loc = 0)
save_fig('ConditionNumber-mesh')
