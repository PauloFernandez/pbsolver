from plotset import *

common_conditions = [('mol_name', mol_name), ('hmat.max_block_size', 2048)]

# ENERGY VS QUADRATURE ORDER
quad_energy_yoon_d4 = extract_results('solvation_energy', common_conditions +
                    [   ('formulation', 'yoon_lenhoff'),
                        ('mesh_density', 4)
                    ])[1:]

quad_energy_juff_d4 = extract_results('solvation_energy', common_conditions + 
                    [   ('formulation', 'juffer'),
                        ('mesh_density', 4)
                    ])[1:]

quadrature_order_d4 = extract_results('quadrature.near.single_order', common_conditions + 
                    [   ('formulation', 'juffer'),
                        ('mesh_density', 4)
                    ])[1:]

quad_energy_yoon_d16 = extract_results('solvation_energy', common_conditions + 
                    [   ('formulation', 'yoon_lenhoff'),
                        ('mesh_density', 16)
                    ])[1:]

quad_energy_juff_d16 = extract_results('solvation_energy', common_conditions + 
                    [   ('formulation', 'juffer'),
                        ('mesh_density', 16)
                    ])[1:]

quadrature_order_d16 = extract_results('quadrature.near.single_order', common_conditions + 
                    [   ('formulation', 'juffer'),
                        ('mesh_density', 16)
                    ])[1:]

# print(quadrature_order_d4)
# quad_energy_juff_d4 = [energy for energy, quad in sorted(zip(quad_energy_juff_d4, quadrature_order_d4))]
# quad_energy_yoon_d4 = [energy for energy, quad in sorted(zip(quad_energy_yoon_d4, quadrature_order_d4))]
# quadrature_order_d4 = sorted(quadrature_order_d4)

near_quad = plt.figure().add_subplot(111)

near_quad.plot(quadrature_order_d4, quad_energy_yoon_d4, marker='o', label='YL d4', color='k')
near_quad.plot(quadrature_order_d4, quad_energy_juff_d4, marker='s', label='Juffer d4', color='k')

#near_quad.plot(quadrature_order_d16, quad_energy_yoon_d16, marker='x', label='YL d16', color='k')
#near_quad.plot(quadrature_order_d16, quad_energy_juff_d16, marker='^', label='Juffer d16', color='k')

#near_quad.set_title('Mesh Convergence')
near_quad.set_xlabel('Near Quadrature Order')
near_quad.set_ylabel(r'$\Delta$G  [kcal/mol]')
near_quad.xaxis.set_major_locator(MaxNLocator(integer=True))
near_quad.legend()
save_fig('NearQuadOrder')
