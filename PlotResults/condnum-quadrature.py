from plotset import *

common_conditions = [('mol_name', mol_name), ('hmat.max_block_size', 2048)]

# CONDITION NUMBER VS QUADRATURE ORDER
cond_num_yoon_d2 = extract_results('num_cond',
                        common_conditions 
                        + [   
                            ('formulation', 'yoon_lenhoff'),
                            ('mesh_density', 2)
                        ])

cond_num_juff_d2 = extract_results('num_cond', 
                        common_conditions 
                        + [   
                            ('formulation', 'juffer'),
                            ('mesh_density', 2)
                        ])

cond_num_yoon_d8 = extract_results('num_cond', 
                        common_conditions 
                        + [   
                            ('formulation', 'yoon_lenhoff'),
                            ('mesh_density', 8)
                        ])

cond_num_juff_d8 = extract_results('num_cond',
                        common_conditions 
                        + [   
                            ('formulation', 'juffer'),
                            ('mesh_density', 8)
                        ])

quadrature_order_d2 = extract_results('quadrature.near.single_order',
                        common_conditions 
                        + [   
                            ('formulation', 'juffer'),
                            ('mesh_density', 2)
                        ])

quadrature_order_d8 = extract_results('quadrature.near.single_order',
                        common_conditions 
                        + [   
                            ('formulation', 'juffer'),
                            ('mesh_density', 8)
                        ])

cond_numb_quad_order = plt.figure().add_subplot(111)

cond_numb_quad_order.semilogy(quadrature_order_d2, cond_num_yoon_d2, marker='o', label='YL d2', color='k')
cond_numb_quad_order.semilogy(quadrature_order_d2, cond_num_juff_d2, marker='s', label='Juffer d2', color='k')

cond_numb_quad_order.semilogy(quadrature_order_d8, cond_num_yoon_d8, marker='x', label='YL d8', color='k')
cond_numb_quad_order.semilogy(quadrature_order_d8, cond_num_juff_d8, marker='^', label='Juffer d8', color='k')

#cond_numb_quad_order.set_title('Mesh Convergence')
cond_numb_quad_order.set_xlabel('Quadrature Order')
cond_numb_quad_order.set_ylabel(r'Condition Number')
cond_numb_quad_order.xaxis.set_major_locator(MaxNLocator(integer=True))
cond_numb_quad_order.legend()
save_fig('ConditionNumber-QO')
