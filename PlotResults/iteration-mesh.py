from plotset import *

common_conditions = [('mol_name', mol_name), ('quadrature.near.single_order', 4), ('hmat.max_block_size', 2048)]

# ITERATIONS VS NUMBER of ELEMENTS
n_boundary_yoonlh = extract_results('number_of_elements', 
                        common_conditions + 
                        [('formulation', 'yoon_lenhoff')])

n_boundary_juffer = extract_results('number_of_elements', 
                        common_conditions + 
                        [('formulation', 'juffer')])

iterations_yoonlh = extract_results('iterations', 
                        common_conditions + 
                        [('formulation', 'yoon_lenhoff')])

iterations_juffer = extract_results('iterations',
                        common_conditions +
                        [('formulation', 'juffer')])

nmbr_it = plt.figure().add_subplot(111)

nmbr_it.plot(n_boundary_yoonlh, iterations_yoonlh, marker='o', label='YL', color='k')
nmbr_it.plot(n_boundary_juffer, iterations_juffer, marker='s', label='Juffer', color='k')

nmbr_it.set_xlabel('N of Elements')
nmbr_it.set_ylabel('GMRES Iterations')
nmbr_it.xaxis.set_major_locator(MaxNLocator(integer=True))
nmbr_it.yaxis.set_major_locator(MaxNLocator(integer=True))
nmbr_it.legend(loc = 0)
save_fig('Iterations')
