from plotset import *

common_conditions = [('hmat.max_block_size', 2048)]

# QUADRATURE ORDER VS NUMBER OF ELEMENTS
n_elements_yl = extract_results('mesh_density', common_conditions + [('formulation', 'yoon_lenhoff')])
n_elements_jf = extract_results('mesh_density', common_conditions + [('formulation', 'juffer')])

quadratures_yl = extract_results('quadrature.near.single_order', common_conditions + [('formulation', 'yoon_lenhoff')])
quadratures_jf = extract_results('quadrature.near.single_order', common_conditions + [('formulation', 'juffer')])

scatter = plt.figure().add_subplot(111)
#scatter.scatter(n_elements_yl, quadratures_yl, marker='o', label='YL', color='k')
scatter.scatter(n_elements_jf, quadratures_jf, marker='x', label='JF', color='k')

scatter.set_xlabel('Number of Elements')
scatter.set_ylabel('Quadrature Order')
scatter.legend()
save_fig('QuadScatter')