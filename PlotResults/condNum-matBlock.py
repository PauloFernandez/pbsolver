from plotset import *

common_conditions = [('mol_name', mol_name), ('quadrature.near.single_order', 4)]

# CONDITION NUMBER VS MATRIX DENSITY
condnum_yoonlh = extract_results('num_cond',
                        common_conditions +
                        [('formulation', 'yoon_lenhoff')])

condnum_juffer = extract_results('num_cond',
                        common_conditions + 
                        [('formulation', 'juffer')])

max_block_size_yl = extract_results('hmat.max_block_size',
                        common_conditions +
                        [('formulation', 'yoon_lenhoff')])

max_block_size_jf = extract_results('hmat.max_block_size',
                        common_conditions +
                        [('formulation', 'juffer')])

print(condnum_juffer)


cond_numb = plt.figure().add_subplot(111)

#cond_numb.plot(condnum_yoonlh, max_block_size_yl, marker='o', label='msms YL', color='k')
cond_numb.plot(max_block_size_jf, condnum_juffer, marker='s', label='msms Juffer', color='k')

cond_numb.set_xlabel('Number of Elements')
cond_numb.set_ylabel('Condition Number')
cond_numb.legend(loc = 0)
save_fig('ConditionNumber-MaxBlockSize')
