import numpy as np
import json

import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator
plt.switch_backend('agg')

params = {  'figure.figsize':  (7, 5),
            'font.size': 12,
            'axes.titlesize':  10,
            'axes.labelsize':  10,
            'xtick.labelsize': 8,
            'ytick.labelsize': 8,
            'font.family': 'serif',
            'legend.fontsize': 8,
            'lines.linewidth': 0.5,
            'lines.color': 'k',
            'legend.loc': 'best',
            'lines.markersize': 4
          }
plt.rcParams.update(params)

mol_name = 'cube'
log_file = '../log.json'

with open(log_file) as infile:
    log = json.load(infile)

def extract_results(data, conditions):
    global log

    def prefix(element):
        for section in log[0]:
            if element in log[0][section]:
                return section
        print('No prefix found for {}'.format(element))

    def its_ok(run, conditions):
        satisfy = True
        
        for parameter, value in conditions:
            satisfy = satisfy and run[prefix(parameter)][parameter] == value

        return satisfy

    return [run[prefix(data)][data] for run in log if its_ok(run, conditions)]

def save_fig(plot_name):
	global mol_name
	plt.savefig('{}-{}.png'.format(mol_name.upper(), plot_name), bbox_inches = 'tight', pad_inches = 0)