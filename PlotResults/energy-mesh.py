import sys
sys.path.append('..')
from PB import richardson
from plotset import *

common_condition = [('mol_name', mol_name), ('quadrature.near.single_order', 4)]

n_boundary_yoonlh = extract_results('number_of_elements', 
                    common_condition + 
                    [('formulation', 'yoon_lenhoff')])

n_boundary_juffer = extract_results('number_of_elements', 
                    common_condition + 
                    [('formulation', 'juffer')])

energy_yoon = extract_results('solvation_energy',
                    common_condition + 
                    [('formulation', 'yoon_lenhoff')])

energy_juff = extract_results('solvation_energy',
                    common_condition + 
                    [('formulation', 'juffer')])


rem_yl = [2,4,5,6,7,8,9,10]
rem_jf = [2]

for i in rem_yl[::-1]:
    del n_boundary_yoonlh[i]
    del energy_yoon[i]

for i in rem_jf[::-1]:
    del n_boundary_juffer[i]
    del energy_juff[i]

print(energy_yoon)
print(energy_juff)
print(n_boundary_yoonlh)
print(n_boundary_juffer)

rich_energy_yoon, r_y, p_y = richardson.extrapolation(energy_yoon, n_boundary_yoonlh)
rich_energy_juff, r_j, p_j = richardson.extrapolation(energy_juff, n_boundary_juffer)

print('yoon: ',rich_energy_yoon, r_y, p_y)
print('juff: ',rich_energy_juff, r_j, p_j)

energy_rslt = plt.figure().add_subplot(111)
#energy_rslt.loglog(n_boundary_yoonlh, np.abs(energy_yoon), marker='o', label='yoonlh', color='k')
#energy_rslt.loglog(n_boundary_juffer, np.abs(energy_juff), marker='s', label='Juffer', color='k')

energy_rslt.plot(n_boundary_yoonlh, energy_yoon, marker='o', label='YL', color='k')
energy_rslt.plot(n_boundary_juffer, energy_juff, marker='s', label='Juffer', color='k')

r_solution_d = rich_energy_yoon*np.ones(len(n_boundary_yoonlh))
r_solution_j = rich_energy_juff*np.ones(len(n_boundary_juffer))

energy_rslt.plot(n_boundary_yoonlh, r_solution_d, 'r--', color='k')
energy_rslt.plot(n_boundary_juffer, r_solution_j, 'r--', color='k')

energy_rslt.set_title('Mesh Convergence')
energy_rslt.set_xlabel('N of Elements')
energy_rslt.set_ylabel(r'$\Delta$G  [kcal/mol]')
energy_rslt.legend()
save_fig('Energy')
