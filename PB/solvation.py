import bempp.api
import numpy as np
import json
import os

from time import time

import sys
sys.path.append('..')
from parameters import *
import linear_system
from formulation import yoon_lenhoff, juffer


def energy(formulation_used, dirichl_space, neumann_space, q, x_q, nummat=False):

    times = {}
    times['total'] = time()

    print("  Matrix Assambling")
    if formulation_used == 'yoon_lenhoff':
        A, rhs, times['assambly_matrix'] = yoon_lenhoff(dirichl_space, neumann_space, q, x_q)
    elif formulation_used == 'juffer':
        A, rhs, times['assambly_matrix'] = juffer(dirichl_space, neumann_space, q, x_q)
    else:
        print("Error!: Formulation not match")
        exit()

    #print('    Number of blocks', bempp.api.hmatrix_interface.number_of_blocks(A))

    if nummat:
        print("  Obtaining Numerical Matrix")
        times['numerical_matrix'] = time()
        A_num = bempp.api.as_matrix(A)
        condition_number = np.linalg.cond(A_num)
        times['numerical_matrix'] = time() - times['numerical_matrix']
    else:
        times['numerical_matrix'] = 0
        condition_number = 0

    print("  GMRES Solving")
    x, info, times['solver'], it_count = linear_system.solver(A, rhs)
    
    solv_energy = energy_from_solution(x, dirichl_space, neumann_space, q, x_q)

    times['total'] = time() - times['total']
    
    return  solv_energy, condition_number, times, it_count


def energy_from_solution(x, dirichl_space, neumann_space, q, x_q):
    solution_dirichl = bempp.api.GridFunction(dirichl_space, 
                                              coefficients=x[:dirichl_space.global_dof_count])
    solution_neumann = bempp.api.GridFunction(neumann_space, 
                                              coefficients=x[dirichl_space.global_dof_count:])
    #solution_dirichl.plot()

    from bempp.api.operators.potential.laplace import single_layer, double_layer

    slp_q = single_layer(neumann_space, x_q.transpose())
    dlp_q = double_layer(dirichl_space, x_q.transpose())
    phi_q = slp_q*solution_neumann - dlp_q*solution_dirichl

    # total solvation energy applying constant to get units [kcal/mol]
    total_energy = 2*np.pi*332.064*np.sum(q*phi_q).real
    print("  Total solvation energy: {:13.8f} [kcal/mol]".format(total_energy))

    return total_energy