import bempp.api
import numpy as np
import inspect
import sys

from time import time
from scipy.sparse.linalg import gmres

array_it, array_frame, it_count = np.array([]), np.array([]), 0

def solver(A, rhs):

    time_solver = time()

    def iteration_counter(x):
        global array_it, array_frame, it_count
        it_count += 1
        frame, array_it = inspect.currentframe().f_back, np.append(array_it, it_count)
        frame_flocals = frame.f_locals["resid"]
        array_frame = np.append(array_frame, frame_flocals)
        sys.stdout.flush()
        sys.stdout.write("    Iter: {0} - Error: {1:.2E}    \r".format(it_count, frame_flocals))       

    x, info = gmres(A, rhs, callback=iteration_counter, tol=1e-3, maxiter=500, restart=1000)
    print("  The linear system was solved in {0} iterations".format(it_count))

    time_solver = time() - time_solver

    return x, info, time_solver, it_count
