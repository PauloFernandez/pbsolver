import numpy as np

def extrapolation(values, n_of_elements):
    r = n_of_elements[-1]/n_of_elements[-2]
    p = np.log((values[-3] - values[-2])/(values[-2] - values[-1]))/np.log(r)
    f_rich = values[-1] + (values[-1] - values[-2])/(r**p - 1)  #f1 fine, f2 coarse

    return f_rich, r, p
