"""Functions to parse pqr strings and read pqr files directly."""

import numpy as np


def pqr_read(file):
    """Read and parse a pqr string."""
    with open(file, "r") as pqr_file:
        pqr_string = pqr_file.read()

    return pqr_parse(pqr_string)


def pqr_parse(pqr_string):
    """Parse a pqr string."""
    x_q = np.empty((0, 3))
    q = np.array([])

    for line in pqr_string.split("\n"):
        if "ATOM" in line:
            line = line.split()
            coordinates = np.array(line[6:9]).astype(float)
            charge = line[9]

            x_q = np.vstack((x_q, coordinates))
            q = np.append(q, float(charge))

    return q, x_q
